const express       = require('express');
const MongoClient   = require('mongodb').MongoClient;
const bodyParser    = require('body-parser');
const dbconf        = require('./config/db');
const app           = express();

const playfield     = require('./app/playfield');

const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));

MongoClient.connect(dbconf.url, (err, client) => {
    if (err) return console.log(err)
                        
    // Make sure you add the database name and not the collection name
    var db = client.db("note-api"); 
    //strange - are we overriding a constant with a database handle or creating a local variable that is just them passed to the routes require?
   
    //debug start
    // db.collection('notes').findOne({},function(findErr,result){
    //     if (findErr) throw findErr;
    //     console.log(`Found result:${result}`);
    //     client.close();
    // })
    //debug end

    require('./app/routes')(app, db); //https://stackoverflow.com/questions/35813584/invalid-schema-expected-mongodb
    //fixed collection is not a function error https://stackoverflow.com/questions/47662220/db-collection-is-not-a-function-when-using-mongoclient-v3-0/47662979
    app.listen(port, () => {
      console.log('We are live on ' + port);
    });
})