const defaults = require('../config/defaults');

var current_width = defaults.default_playfield_width;
var current_height = defaults.default_playfield_height;
const OPF = 3; //octets per field

var mainfield = Buffer.alloc(current_height*current_width*OPF); //default initial size during start of server
var nextfield;


module.exports = {
    shrink_playfield : function(w, h) { 
        //resize will be always symmetrical in both directions from the center
        //thus, when growing by 1 pixel, one will be added 'to the left' and one 'to the right' etc
        var tempfield = Buffer.alloc(mainfield.length);
        mainfield.copy(tempfield);

        new_width = current_width-(2*w);
        new_height = current_height-(2*h);
        mainfield.alloc(new_height*new_width*OPF);

        for(y = 0; y<new_height; y++) {
            tempfield.copy(mainfield,
                          (y*new_width*OPF),
                          ((y+h)*current_width+w)*OPF,
                          ((y+h)*current_width+w+new_width)*OPF
                        );
        }
        tempfield.alloc(0);
        return mainfield;
    },
    grow_playfield : function(w,h) {
        var tempfield = Buffer.alloc(mainfield.length);
        mainfield.copy(tempfield);

        new_width = current_width-(2*w);
        new_height = current_height-(2*h);
        mainfield.alloc(new_height*new_width*OPF);

        for(y = 0; y<curent_height; y++) {
            tempfield.copy(mainfield,
                          ((y+h)*new_width+w)*OPF,
                          (y*current_width)*OPF,
                          (y*current_width+current_width)*OPF
                        );
        }
        tempfield.alloc(0);
        return mainfield;
        
    },
    get_playfield : function(){
        return mainfield;
        //{ field = mainfield, width = current_width, height = current_height};
    },
    get_width : function(){
        return current_width;
    },
    get_height : function(){
        return current_height;
    },
    preprocess : function() {
        nextfield = Buffer.alloc(mainfield.length);
        return nextfield;
    },

    get_delta : function() {
        //calculate the delta with RLE between mainfield and nextfield and return it
        var deltafield = Buffer.alloc(mainfield.length);
        for (x=0; x<mainfield.length; x++) {
            deltafield[x] = nextfield[x]-mainfield[x];
        }
        //here apply the rle based on something that i'll find in other NPM module
        return deltafield;
    },
    apply_delta : function(deltafield,multiplier) {
        //multiplier 1 is addition, -1 is substraction, fraction can be used for weighted addition or substraction
        for (x=0; x<mainfield.length; x++) {
            mainfield[x] = mainfield[x]+(deltafield[x]*multiplier);
        }
        return mainfield;
    },
    
}