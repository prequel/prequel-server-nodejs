// const noteRoutes = require('./note_routes');
const userRoutes = require('./user_routes');
const systemRoutes = require('./system_routes');
const actionRoutes = require('./action_routes');
module.exports = function(app, db) {
  // noteRoutes(app, db);
  userRoutes(app, db);
  actionRoutes(app, db);
  systemRoutes(app, db);
  // Other route groups could go here, in the future
};