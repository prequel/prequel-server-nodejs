module.exports = function (app, db) {
    app.get('/action/allmine',(req,res)=> { //AUTH, USER (must know userID or supply identity alongside request)
        //will list all planned actions for the current user since the beginning of the season
        //data listed will be: skill used, pos_x, pos_y, power_multiplier, status(int): 0- happened, 1- queued, 2- failed (pos_y was out of bounds)

    });
    app.get('/action/lastx/:slices',(req,res)=> { //AUTH,USER
        //will list all actions that happened (? in a FOW radius around current user's position) in the last X timeslices

        //client side,all the ones already received should be skipped
        //data listed will be all that are required to reconstruct board slices into the past, i.e.:
        //skill used,pos_x,pos_y, power_multiplier
        //this data will be processed client side and recreate board state in those time slices
    });
    app.get('/action/last_specific',(req,res)=> { //AUTH,USER
        //body will contain a list of timeslices to fetch - allows reduced load when traversing history and some slices already received

    }); 
    app.get('/action/details/:action_id',(req,res)=> { //AUTH, USER 
    });
    app.post('/action/queue',(req,res)=> { //AUTH, USER, payload is a list of unprocessed actions planned (filtering which are to be still processed needs to be done by the client)
        //each item in the list is a : timestamp, skill number (multiplier will be determined by the server), pos_x, pos_y
        //if the timestamp will be in future but already planned, the old one planned will be replaced by this one and thus updated
        //returns a list of error codes(binary field): 0 all variables validated, queued command (is in future, skill exists (0-8), pos_x is within current bounds
        //1 - timestamp in past or current - can only plan future
        //2 - unknown skill (<0 or >8) !!! skill 7 is idle/skip turn, skill 8 is move

        //4 - pos_x is out bounds (pos_y is limited but looping around)
        //8 - invalid move pos
        
    });
    
    app.post('/action/idleall',(req,res)=> { //AUTH, USER - will delete all planned actions, processing will treat this player as idling
    });
    app.post('/action/idle/:timestamp',(req,res)=> { //AUTH, USER - will replace specific action with idle
    });
    
};